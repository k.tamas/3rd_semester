﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRun : MonoBehaviour
{
    
    [SerializeField] private float UpForce;
    [SerializeField] private float SideForce;
    [SerializeField] private float DownForce;
    [SerializeField] private float RayDist;
    [SerializeField] private float CamRotation;

    private Rigidbody rb;

    public Transform Head;
    public Transform Cam;

    private bool IsLeft;
    private bool IsRight;

    public bool IsWallRunning;

    private float DistLeft;
    private float DistRight;

    public float WallRunTime;
    private float CurrentWallRunTime;

    private void Awake() {
        rb = GetComponent<Rigidbody>();

    }

    private void Update() {
        CheckWall();
        //CheckIfWallRunning();
    }

    private void CheckWall() {
        RaycastHit left;
        RaycastHit right;

        if (Physics.Raycast(Head.position, Head.right, out right, RayDist)) {
            DistRight = Vector3.Distance(Head.position, right.point);
            if(DistRight < 3f) {
                IsRight = true;
                IsLeft = false;
            }
        }

        if (Physics.Raycast(Head.position, -Head.right, out left, RayDist)) {
            DistLeft = Vector3.Distance(Head.position, left.point);
            if (DistLeft < 3f) {
                IsRight = false;
                IsLeft = true;
            }
        }
    }

    void OnCollisionEnter(Collision collison) {
        if (collison.transform.CompareTag("Wall")) {
            rb.useGravity = false;
      
            IsWallRunning = true;

            if (IsRight) {
                Cam.localEulerAngles = Vector3.Lerp(Cam.localEulerAngles, new Vector3(0f, 0f, CamRotation), 10f);
                rb.AddForce(Vector3.down * DownForce * Time.deltaTime);
            }

            if (IsLeft) {
                Cam.localEulerAngles = Vector3.Lerp(Cam.localEulerAngles, new Vector3(0f, 0f, -CamRotation), 10f);
                rb.AddForce(Vector3.down * DownForce * Time.deltaTime);
            }
        }
    }

    void OnCollisionStay(Collision collision) {
        if (collision.transform.CompareTag("Wall")) {
            
            if(Input.GetKey(KeyCode.Space)) {
                if (IsLeft) {
                    rb.AddForce(Vector3.up * UpForce * Time.deltaTime);
                    rb.AddForce(Head.right * SideForce);
                }
                if (IsRight) {
                    rb.AddForce(Vector3.up * UpForce * Time.deltaTime);
                    rb.AddForce(-Head.right * SideForce);
                }
            }
        }
    }

    void OnCollisionExit(Collision collision) {
        IsWallRunning = false;
        if (collision.transform.CompareTag("Wall")) {
            rb.useGravity = true;
            Cam.localEulerAngles = Vector3.Lerp(Cam.localEulerAngles, new Vector3(0f, 0f, 0f), 2f);
        }
    }

    public void CheckIfWallRunning() {
        if (IsLeft || IsRight) 
            IsWallRunning = true;
       
        else
            IsWallRunning = false;
    }
}
