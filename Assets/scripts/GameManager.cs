﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public string playerPrefab;
    public Transform[] spawnPoints;

    private void Start() {
        Spawn();
    }

    public void Spawn() {
        Transform tempSpawn = spawnPoints[Random.Range(0, spawnPoints.Length)];
        PhotonNetwork.Instantiate(playerPrefab, tempSpawn.position, tempSpawn.rotation);
    }
}
