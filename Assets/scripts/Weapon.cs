﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using UnityEngine.UI;

public class Weapon : MonoBehaviourPunCallbacks {


    public Gun[] loadout;
    public Transform weaponParent;

    private int currentIndex;
    private GameObject currentEquipment;

    public float currentCoolDown;
    public GameObject bulletHolePrefab;
    public LayerMask canBeShot;

    private bool isReloading = false;

	// Use this for initialization
	void Start () {
        if (photonView.IsMine) {
            foreach (Gun g in loadout)
                g.Init();
        }
        Equip(0);
	}
	
	// Update is called once per frame
	void Update () {

        if (photonView.IsMine && Input.GetKeyDown(KeyCode.Alpha1))
            photonView.RPC("Equip", RpcTarget.All, 0);

        if (currentEquipment != null) {
            if (photonView.IsMine) {

                Aim(Input.GetMouseButton(1));

                if (Input.GetMouseButtonDown(0) && currentCoolDown <= 0)
                    if (loadout[currentIndex].CanFireBullet())
                        photonView.RPC("Shoot", RpcTarget.All);
                    else StartCoroutine(Reload(loadout[currentIndex].reloadTime));

                if (Input.GetKeyDown(KeyCode.R))
                    StartCoroutine(Reload(loadout[currentIndex].reloadTime));


                if (currentCoolDown > 0)
                    currentCoolDown -= Time.deltaTime;
            }



        currentEquipment.transform.localPosition = Vector3.Lerp(currentEquipment.transform.localPosition, Vector3.zero, Time.deltaTime * 4f);
            
        }

        
	}

    IEnumerator Reload(float pWait) {
        loadout[currentIndex].Reload();
        if (loadout[currentIndex].canReload) {
            isReloading = true;

            currentEquipment.SetActive(false);

            yield return new WaitForSeconds(pWait);

            
            currentEquipment.SetActive(true);
            isReloading = false;
        }
        
    }

    [PunRPC]
    void Equip(int p_ind){

        if (currentEquipment != null) {
            if(isReloading)
                StopCoroutine("Reload");
            Destroy(currentEquipment);
        }

        currentIndex = p_ind;

        GameObject newEquipment = Instantiate(loadout[p_ind].prefab, weaponParent.position, weaponParent.rotation, weaponParent) as GameObject;
        newEquipment.transform.localPosition = Vector3.zero;
        newEquipment.transform.localEulerAngles = Vector3.zero;
        newEquipment.GetComponent<Sway>().isMine = photonView.IsMine;

        currentEquipment = newEquipment;
 
    }

    void Aim(bool isAiming){

        Transform anchor = currentEquipment.transform.Find("anchor");
        Transform stateAds = currentEquipment.transform.Find("states/ads");
        Transform stateHip = currentEquipment.transform.Find("states/hip");

        if (isAiming){
            anchor.position = Vector3.Lerp(anchor.position, stateAds.position, Time.deltaTime * loadout[currentIndex].aimSpeed);
        }else{
            anchor.position = Vector3.Lerp(anchor.position, stateHip.position, Time.deltaTime * loadout[currentIndex].aimSpeed);
        }

    }

    [PunRPC]
    void Shoot(){

        Transform spawn = transform.Find("Cameras/PlayerCameraNormal");

        Vector3 bloom = spawn.position + spawn.forward * 1000f;
        bloom += Random.Range(-loadout[currentIndex].bloom, loadout[currentIndex].bloom) * spawn.up;
        bloom += Random.Range(-loadout[currentIndex].bloom, loadout[currentIndex].bloom) * spawn.right;
        bloom -= spawn.position;
        bloom.Normalize();

        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(spawn.position, bloom, out hit, 1000f, canBeShot)) {
            Vector3 hitPoint = new Vector3(hit.point.x, hit.point.y - 1.5f, hit.point.z);

            if (hit.collider.gameObject.layer != 12) {
                GameObject newHole = Instantiate(bulletHolePrefab, hitPoint + hit.normal * 0.001f, Quaternion.identity) as GameObject;
                newHole.transform.LookAt(hitPoint + hit.normal * 1f);
                Destroy(newHole, 5f);
            }

            if(photonView.IsMine)
                if(hit.collider.gameObject.layer == 12) 
                    {
                        hit.collider.transform.root.gameObject.GetPhotonView().RPC("TakeDmg", RpcTarget.All, loadout[currentIndex].dmg);
                    }

        }

        currentEquipment.transform.Rotate(-loadout[currentIndex].recoil, 0, 0);
        currentEquipment.transform.position -= currentEquipment.transform.forward * loadout[currentIndex].kickBack;

        currentCoolDown = loadout[currentIndex].fireRate;

    }

     [PunRPC]
     private void TakeDmg(int p_dmg) {
        GetComponent<Player>().TakeDmg(p_dmg);
    }


    public void RefreshAmmo(Text pText) {
        int clip = loadout[currentIndex].GetClip();
        int stash = loadout[currentIndex].GetStash();

        pText.text = clip.ToString("D2") + " / " + stash.ToString("D2");
    }
}
