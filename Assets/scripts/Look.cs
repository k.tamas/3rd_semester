﻿using UnityEngine;
using System.Collections;
using Photon.Pun;

public class Look : MonoBehaviourPunCallbacks {

    public static bool cursorLocked = true;

    public Transform player;
    public Transform cam;
    public Transform weapon;

    public float sensX;
    public float sensY;
    public float maxAngle;

    private Quaternion camCenter;

	// Use this for initialization
	void Start () {
        camCenter = cam.localRotation;
	}
	
	// Update is called once per frame
	void Update () {

        if (!photonView.IsMine)
            return;

        SetY();
        SetX();

        UpdateCursosLock();
	}

    void SetY() {
        float tInput = Input.GetAxis("Mouse Y") * sensY * Time.deltaTime;
        Quaternion tAdj = Quaternion.AngleAxis(tInput, -Vector3.right);
        Quaternion tDelta = cam.localRotation * tAdj;

        if(Quaternion.Angle(camCenter, tDelta) < maxAngle)
                cam.localRotation = tDelta;

        weapon.rotation = cam.rotation;
    }

    void SetX() {
        float tInput = Input.GetAxis("Mouse X") * sensX * Time.deltaTime;
        Quaternion tAdj = Quaternion.AngleAxis(tInput, Vector3.up);
        Quaternion tDelta = player.localRotation * tAdj;

        player.localRotation = tDelta;
    }


    void UpdateCursosLock() {
        if (cursorLocked) {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            if (Input.GetKeyDown(KeyCode.Escape))
                cursorLocked = false;
        }
        else {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            if (Input.GetKeyDown(KeyCode.Escape))
                cursorLocked = true;
        }
            
    }
}
