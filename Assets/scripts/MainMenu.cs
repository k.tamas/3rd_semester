﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviourPunCallbacks
{

    public void Awake() {
        PhotonNetwork.AutomaticallySyncScene = true;
        Connect();
    }

    public override void OnConnectedToMaster() {
        
        base.OnConnectedToMaster();
    }

    public override void OnJoinedRoom() {
        StartGame();

        base.OnJoinedRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message) {
        Create();

        base.OnJoinRandomFailed(returnCode, message);
    }

    public void Connect() {
        PhotonNetwork.GameVersion = "d0.0.2";
        PhotonNetwork.ConnectUsingSettings();
    }

    public void Join() {
        PhotonNetwork.JoinRandomRoom();
    }

    public void Create() {
        PhotonNetwork.CreateRoom("");
    }

    public void StartGame() {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            PhotonNetwork.LoadLevel(1);

    }

    public void JoinMatch() {
        Join();
    }

    public void CreateMatch() {
        Create();
    }

    public void Options() {
        SceneManager.LoadScene(2);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
