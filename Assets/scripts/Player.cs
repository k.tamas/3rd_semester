﻿using UnityEngine;
using System.Collections;
using Photon.Pun;
using UnityEngine.UI;

public class Player : MonoBehaviourPunCallbacks {

    public KeyCode sprintKey = KeyCode.LeftShift;
    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode slideKey = KeyCode.LeftControl;
    public KeyCode crouchKey = KeyCode.LeftControl;
    public KeyCode hookKey = KeyCode.E;

    public float speed;
    public float sprintMod;
    public float slideMod;
    public float hookMod;
    public float crouchMod;
    public float wallRunMod;
    public float hookSpeed;
    public float jumpForce;
    public float slideLength;
    public float hookDistance;
    public int maxHealth;
    public Camera normalCam;
    public GameObject cameraParent;
    public Transform weaponParent;
    public Transform groundDetector;
    public Transform Hook;
    public LayerMask ground;

    public float slideAmount;
    public float crouchAmount;
    public GameObject standingCollider;
    public GameObject crouchCollider;
    public GameObject standingEyeCollider;

    private WallRun WallRun;

    private Transform uiHeathBar;
    private Text uiAmmo;
    private Image uiCrosshair;
    
    private Rigidbody rb;

    private Vector3 targetBobPos;
    private Vector3 weaponParentOrg;
    private Vector3 weaponParentCurrPos;

    private float movementCounter;
    private float idleCounter;
    private float hookSize;

    private float baseFov;
    private float sprintFovMod = 1.2f;
    private float slideFovMod = 1.5f;
    private Vector3 camOrigin;

    private int currentHealth;


    private GameManager gameManager;
    private Weapon weapon;

    private bool inHook;
    private bool hookThrown;
    private bool sliding;
    private bool crouching;
    private float slideTime;
    
    private Vector3 slideDir;

    private Vector3 HookPos;


	// Use this for initialization
	void Start () {

        Hook.gameObject.SetActive(false);

        WallRun = GetComponent<WallRun>();

        gameManager = GameObject.Find("Manager").GetComponent<GameManager>();
        weapon = GetComponent<Weapon>();

        cameraParent.SetActive(photonView.IsMine);

        if (!photonView.IsMine) {
            gameObject.layer = 12;
            standingCollider.gameObject.layer = 12;
            crouchCollider.gameObject.layer = 12;
        }

        baseFov = normalCam.fieldOfView;
        camOrigin = normalCam.transform.localPosition;
    
        if(Camera.main)
            Camera.main.enabled = false;

        rb = GetComponent<Rigidbody>();
        weaponParentOrg = weaponParent.localPosition;
        weaponParentCurrPos = weaponParentOrg;

        currentHealth = maxHealth;

        if (photonView.IsMine) {
            uiHeathBar = GameObject.Find("HUD/Health/Bar").transform;
            uiAmmo = GameObject.Find("HUD/Ammo/Text").GetComponent<Text>();
            uiCrosshair = GameObject.Find("HUD/Crosshair/Crosshair").GetComponent<Image>();
            UpdateHealth();
        }

    }

    private void Update() {

        if (!photonView.IsMine)
            return;


        HookShotStart();

        if (hookThrown) {

            HookThrow();
        }

        if (inHook) {

            HandleHooKMovement();
            rb.useGravity = false;
        }
        else
            rb.useGravity = true;


        if (Input.GetKeyDown(KeyCode.P))
            Die();



        //Axes
        float tHMove = Input.GetAxisRaw("Horizontal");
        float tVMove = Input.GetAxisRaw("Vertical");

        //Control
        bool tSprint = Input.GetKey(sprintKey);
        bool tJump = Input.GetKeyDown(jumpKey);
        bool tCrouch = Input.GetKeyDown(crouchKey);


        //States
        bool isGrounded = Physics.Raycast(groundDetector.position, Vector3.down, 0.15f, ground);
        bool isJumping = tJump && isGrounded;
        bool isSprinting = tSprint && tVMove > 0 && !isJumping && isGrounded;
        bool isCrouching = tCrouch && !isSprinting && !isJumping && isGrounded;

        //Crouching
        if (isCrouching)
            photonView.RPC("SetCrouch", RpcTarget.All, !crouching);

        //Jumping
        if (isJumping) {
            if(crouching)
                photonView.RPC("SetCrouch", RpcTarget.All, false);
            rb.AddForce(Vector3.up * jumpForce);
        }

        //Head movement
        bool tIsAds = Input.GetMouseButton(1);
        if (!tIsAds) {
            if(!uiCrosshair.enabled)
                uiCrosshair.enabled = true;

            if (tHMove == 0 && tVMove == 0 && !sliding) {
                HeadBob(idleCounter, 0.025f, 0.025f);
                idleCounter += Time.deltaTime;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetBobPos, Time.deltaTime * 2f);
            }
            else if (!isSprinting && !crouching) {
                HeadBob(movementCounter, 0.05f, 0.05f);
                movementCounter += Time.deltaTime * 2f;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetBobPos, Time.deltaTime * 8f);
            }
            else if (crouching) {
                HeadBob(movementCounter, 0.015f, 0.015f);
                movementCounter += Time.deltaTime * 3f;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetBobPos, Time.deltaTime * 6f);
            }
            else {
                HeadBob(movementCounter, 0.2f, 0.2f);
                movementCounter += Time.deltaTime * 2f;
                weaponParent.localPosition = Vector3.Lerp(weaponParent.localPosition, targetBobPos, Time.deltaTime * 10f);
            }
        }
        else {
            weaponParent.localPosition = weaponParentCurrPos;
            if (uiCrosshair.enabled)
                uiCrosshair.enabled = false;
        }

        UpdateHealth();
        weapon.RefreshAmmo(uiAmmo);
        
            
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (!photonView.IsMine)
            return;

        //Axes
        float tHMove = Input.GetAxisRaw("Horizontal");
        float tVMove = Input.GetAxisRaw("Vertical");


        //Control
        bool tSprint = Input.GetKey(sprintKey);
        bool tJump = Input.GetKeyDown(jumpKey);
        bool tSlide = Input.GetKey(slideKey);

        //States
        bool isGrounded = Physics.Raycast(groundDetector.position, Vector3.down, 0.1f, ground);
        bool isJumping = tJump && isGrounded;
        bool isSprinting = tSprint && tVMove > 0 && !isJumping && isGrounded;
        bool isSliding = isSprinting && tSlide && !sliding;
        bool isWallRunning = WallRun.IsWallRunning;
        



        //Movewment
        Vector3 tDirection = Vector3.zero;
        float tAdjSpeed = speed;

        

        if (!sliding) {
            tDirection = new Vector3(tHMove, 0, tVMove);
            tDirection.Normalize();
            tDirection = transform.TransformDirection(tDirection);

            tAdjSpeed = speed;
            if (isSprinting) {
                if (crouching)
                    photonView.RPC("SetCrouch", RpcTarget.All, false);
                tAdjSpeed *= sprintMod;

            }
            else if (crouching)
                tAdjSpeed *= crouchMod;

            else if (isWallRunning) {
                tAdjSpeed *= wallRunMod;
                
            }

        }
        else {
            tDirection = slideDir;
            tAdjSpeed *= slideMod;
            slideTime -= Time.deltaTime;
            if (slideTime <= 0) {
                sliding = false;
                weaponParentCurrPos += Vector3.up * (slideAmount - crouchAmount);

            }
        }

        Vector3 tTargetVelocity = tDirection * tAdjSpeed * Time.fixedDeltaTime;
        tTargetVelocity.y = rb.velocity.y;
        rb.velocity = tTargetVelocity;

        //Sliding
        if (isSliding) {
            sliding = true;
            slideDir = tDirection;
            slideTime = slideLength;
            //adjust camera
            weaponParentCurrPos += Vector3.down * (slideAmount- crouchAmount);
            
            if(!crouching)
                photonView.RPC("SetCrouch", RpcTarget.All, true);
        }

        //Camera
        if (sliding) {
            normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFov * slideFovMod, Time.deltaTime * 8f);
            normalCam.transform.localPosition = Vector3.Lerp(normalCam.transform.localPosition, camOrigin + Vector3.down * slideAmount, Time.deltaTime * 6f);
        }
        else {
            if (isSprinting)
                normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFov * sprintFovMod, Time.deltaTime * 8f);
            else
                normalCam.fieldOfView = Mathf.Lerp(normalCam.fieldOfView, baseFov, Time.deltaTime * 8f);
            if(crouching)
                normalCam.transform.localPosition = Vector3.Lerp(normalCam.transform.localPosition, camOrigin + Vector3.down * crouchAmount, Time.deltaTime * 6f);
            else
                normalCam.transform.localPosition = Vector3.Lerp(normalCam.transform.localPosition, camOrigin, Time.deltaTime * 6f);
        }
    }

    void UpdateHealth() {
        float tempHealthRatio = (float)currentHealth / (float)maxHealth;
        uiHeathBar.localScale =Vector3.Lerp(uiHeathBar.localScale, new Vector3(tempHealthRatio, 1, 1), Time.deltaTime * 8f);

        
    }

    [PunRPC]
    void SetCrouch(bool state) {
        if (crouching == state)
            return;

        crouching = state;

        if(crouching) {
            standingCollider.SetActive(false);
            crouchCollider.SetActive(true);
            standingEyeCollider.SetActive(false);

            weaponParentCurrPos += Vector3.down * crouchAmount;
        }
        else {
            standingCollider.SetActive(true);
            crouchCollider.SetActive(false);
            standingEyeCollider.SetActive(true);

            weaponParentCurrPos -= Vector3.down * crouchAmount;
        }
    }

    void HeadBob(float pZ, float pXInt, float pYInt) {
        targetBobPos = weaponParentCurrPos + new Vector3(Mathf.Cos(pZ) * pXInt, Mathf.Sin(pZ*2) * pYInt, 0); 
    }

    public void TakeDmg(int p_dmg) {
        if (photonView.IsMine) {
            currentHealth -= p_dmg;

            UpdateHealth();

            
            if (currentHealth <= 0) {
                Die();
                
            }
        }
    }

    public void Die() {
        gameManager.Spawn();
        PhotonNetwork.Destroy(gameObject);
    }

 
    private void HookShotStart() {
        RaycastHit hit;
        if (Input.GetKeyDown(hookKey)) {
            if(Physics.Raycast(normalCam.transform.position, normalCam.transform.forward, out hit)) {
                hookThrown = true;
                inHook = false;
                hookSize = 0f;
                HookPos = hit.point;
            }
    
        }
    }


    private void HookThrow() {

        Hook.gameObject.SetActive(true);

        Hook.LookAt(HookPos);

        hookSize += 80f * Time.deltaTime;
        Hook.localScale = new Vector3(1, 1, hookSize);

        if(hookSize >= Vector3.Distance(transform.position, HookPos)) {
            inHook = true;
            hookThrown = false;
        }
    }


    private void HandleHooKMovement() {
        Hook.LookAt(HookPos);

        Vector3 hookDir = HookPos - transform.position.normalized;

        transform.position = Vector3.Lerp(transform.position, hookDir, Time.deltaTime * hookSpeed * hookMod);

        hookSize = Vector3.Distance(transform.position, HookPos);
        Hook.localScale = new Vector3(1, 1, hookSize);

        if (Vector3.Distance(transform.position, HookPos) < hookDistance) {
            inHook = false;
            hookThrown = false;
            Hook.gameObject.SetActive(false);
            rb.useGravity = true;
        }

    }
}
