﻿using UnityEngine;
using System.Collections;
using Photon.Pun;

public class Sway : MonoBehaviour {

    public float intenstity;
    public float smooth;
    public bool isMine;

    private Quaternion originRotation;


	// Use this for initialization
	void Start () {
        originRotation = transform.localRotation;
	}
	
	// Update is called once per frame
	void Update () {

        updateSway();
	}

    private void updateSway(){

        float x_mouse = Input.GetAxis("Mouse X");
        float y_mouse = Input.GetAxis("Mouse Y");

        if(!isMine) {
            x_mouse = 0;
            y_mouse = 0;
        }

        Quaternion x_adj = Quaternion.AngleAxis(-intenstity * x_mouse, Vector3.up);
        Quaternion y_adj = Quaternion.AngleAxis(intenstity * y_mouse, Vector3.right);
        Quaternion targetRotation = originRotation * x_adj * y_adj;

        transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * smooth);

    }
}
