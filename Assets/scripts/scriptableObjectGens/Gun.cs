﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class Gun : ScriptableObject {

    public string name;
    public GameObject prefab;
    public float bloom;
    public float recoil;
    public float kickBack;
    public float fireRate;
    public float aimSpeed;
    public float reloadTime;

    public int totalAmmo;
    public int clipSize;

    public int dmg;

    private int clip;
    private int stash;

    public bool canReload;

    public void Init() {
        stash = totalAmmo;
        clip = clipSize;
    }

    public bool CanFireBullet() {
        if (clip > 0) {
            clip -= 1;
            return true;
        }
        else
            return false;
    }

    public void Reload() {
        if (stash != 0) {
            canReload = true;
            stash += clip;
            clip = Mathf.Min(clipSize, stash);
            stash -= clip;
        }
        else
            canReload = false;
    }

    public int GetStash() { return stash; }
    public int GetClip() { return clip; }
	
}
